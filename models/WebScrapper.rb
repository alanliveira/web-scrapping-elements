require 'nokogiri'
require 'open-uri'

class WebScrapper
    def getData url, query 
        doc = Nokogiri::HTML(URI.open(url))
        data = []
        doc.css(query).each do |d|
            data.push(d.content)
        end
        return data
    end
end