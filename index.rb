require 'sinatra'
require_relative './models/WebScrapper.rb'

get '/' do
    erb :FormUser
end

post '/results' do 
    @url = params['txtURL']
    @query = params['txtQuery']
    @selector = params['selector']
    @type = params['type']

    webScrapper = WebScrapper.new
    @data = webScrapper.getData(@url, "#{@selector}#{@query}")

    erb :response
end